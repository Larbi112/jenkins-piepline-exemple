package com.capgemini.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Person implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idPerson;
    
	@Column
	private String firstName;
	
	@Column
    private String lastName;
	
	@Column
	private String profession;
	
	@Column
	private String tel;
	
	@Column
	private String address;

	public Person() {
	}

	public Person(Long idPerson, String firstName, String lastName, String profession, String tel, String address) {
		this.idPerson = idPerson;
		this.firstName = firstName;
		this.lastName = lastName;
		this.profession = profession;
		this.tel = tel;
		this.address = address;
	}

	public Long getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(Long idPerson) {
		this.idPerson = idPerson;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Person [idPerson=" + idPerson + ", firstName=" + firstName + ", lastName=" + lastName + ", profession="
				+ profession + ", tel=" + tel + ", address=" + address + "]";
	}
	

	
}

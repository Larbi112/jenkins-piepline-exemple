package com.capgemini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonRestCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonRestCrudApplication.class, args);
	}

}
